console.log(
  "%c Social Media Dashboard",
  "font-size:2.5rem; color:hsl(203, 89%, 53%); text-align:center; "
);
const counters = document.querySelectorAll("[data-value]");
let speed = 0;
counters.forEach((counter) => {
  const updateCounter = () => {
    const value = +counter.getAttribute("data-value");
    const count = +counter.innerHTML;
    if (value < 10) {
      speed = 200;
    }

    if (value < 100) {
      speed = 1000;
    }

    if (value < 200) {
      speed = 250;
    }

    if (value > 300) {
      speed = 300;
    }
    let inc = value / speed;

    if (count < value) {
      counter.innerHTML = Math.ceil(count + inc);
      setTimeout(updateCounter, 1);
    } else {
      count.innerHTML = value;
    }
  };

  updateCounter();
});

const toggleTheme = document.querySelector("#theme-switcher");
const currentMode = document.querySelector("[aria-label='current Mode']");
const htmlElement = document.firstElementChild;
const body = document.querySelector("body");

let storageTheme = null;

const getColorPreference = () => {
  if (localStorage.getItem(storageTheme)) {
    return localStorage.getItem(storageTheme);
  } else {
    return window.matchMedia("(prefers-color-scheme: dark)").matches
      ? "dark"
      : "light";
  }
};

const theme = {
  value: getColorPreference(),
};
toggleTheme?.setAttribute("aria-label", theme.value);

if (theme.value === "dark") {
  htmlElement.setAttribute("data-theme", "dark");
  currentMode.innerHTML = "Dark Mode";
} else {
  htmlElement.setAttribute("data-theme", "light");
  currentMode.innerHTML = "Light Mode";
}

toggleTheme.addEventListener("click", () => {
  const currentTheme = htmlElement.getAttribute("data-theme");
  toggleTheme.setAttribute("aria-label", currentTheme);

  if (currentTheme === "light") {
    htmlElement.setAttribute("data-theme", "dark");
    toggleTheme.setAttribute("aria-label", "dark");
    currentMode.innerHTML = "Dark Mode";
  } else {
    htmlElement.setAttribute("data-theme", "light");
    toggleTheme.setAttribute("aria-label", "light");
    currentMode.innerHTML = "Light Mode";
  }
});
